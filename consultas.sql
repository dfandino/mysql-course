select b.book_id, a.name, b.title
from books as b
join authors as a
    on a.author_id = b.author_id
where a.author_id BETWEEN 1 and 5; 

select c.name, b.title, t.type
from transactions as t 
join books as b 
    on t.book_id = b.book_id
join clients as c 
    on t.client_id = c.client_id
join authors as a 
    on b.author_id = a.author_id
where c.gender = 'F'
    and t.type = 'sell';

-- Cantidad de Libros de cada autor --
SELECT a.author_id, a.name, a.nationality, COUNT(b.book_id)
FROM authors AS a
LEFT JOIN books AS b
  ON b.author_id = a.author_id
WHERE a.author_id BETWEEN 1 AND 5
GROUP BY a.author_id
ORDER BY a.author_id;

-- Qué nacionalidades hay? --
SELECT DISTINCT nationality FROM authors; 

-- Cuántos escritores hay de cada nacionalidad?--
SELECT nationality, COUNT(author_id) AS c_authors
FROM authors
WHERE nationality IS NOT NULL
    AND nationality NOT IN ('RUS', 'AUS')
GROUP BY nationality
ORDER BY c_authors DESC;